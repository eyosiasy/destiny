`define XLEN 32  //Width of register and data bus connected to memory
`define ALEN 19  //Width of address bus

`define PC_INIT_ADDR 0 //Init memeory address (where to go to at reset)
`define PMEM_START_32BIT_WORD_ADDR 0
`define DMEM_START_32BIT_WORD_ADDR 65536
`define INTRPT_SR_ADDR_1 0
`define INTRPT_SR_ADDR_2 0
`define INTRPT_SR_ADDR_3 0
`define INTRPT_SR_ADDR_4 0
`define INTRPT_SR_ADDR_5 0
`define INTRPT_SR_ADDR_6 0
`define INTRPT_SR_ADDR_7 0
`define INTRPT_SR_ADDR_8 0

`define UART_SINGLE_RX_BYTE_MEM_ADDR (73664*4)
`define UART_SINGLE_TX_BYTE_MEM_ADDR (73668*4)

`define DMEM_MAX_ADDR (73663*4)
`define DMEM_MIN_ADDR (65536*4)

`define EXT_PORT_IN_ADDR (73672*4)
`define EXT_PORT_OUT_ADDR (73676*4)

`define UART_PMEM_WRITE_START_WORD 16711935 // = 32'h00FF00FF