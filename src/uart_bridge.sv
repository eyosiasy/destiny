`timescale 1ns / 1ps
`include "defines_inc.sv"

module uart_bridge (

   input                    clk,
   input                    async_rst_n,
   input                    sync_rst,
   
   //
   input    [`ALEN-1:0]     data_mem_addr,
   input                    data_mem_wr_en,
   input    [31:0]          data_mem_wr_data,
   
   // UART
   output                   uart_tx,
   input                    uart_rx,

   // Program memory interface
   output                   start_programming_pulse, 
   output                   end_programming_pulse, 
   output                   prog_mem_sync_rst,
   output                   prog_mem_wr_en,
   output   [`ALEN-1:0]     prog_mem_addr,
   output   [`XLEN-1:0]     prog_mem_wr_data

);

// UART TX
logic uart_tx_done;
logic uart_tx_active; 
logic uart_tx_data_in_vld;
logic uart_tx_data_in;

// UART TX (Target --> Host PC)
always_ff @ ( posedge clk or negedge async_rst_n ) begin
    if ( ~async_rst_n ) begin
        uart_tx_data_in_vld <= 0;
        uart_tx_data_in <= 0;
    end 
    else if ( sync_rst ) begin
        uart_tx_data_in_vld <= 0;
        uart_tx_data_in <= 0;
    end
    else if ( data_mem_wr_en & ( data_mem_addr == `UART_SINGLE_TX_BYTE_MEM_ADDR ) ) begin
        uart_tx_data_in_vld <= 1;
        uart_tx_data_in <= data_mem_wr_data[7:0];        
    end 
    else begin
        uart_tx_data_in_vld <= 0;
        uart_tx_data_in <= 0;    
    end
end
         
uart_tx #(.CLKS_PER_BIT(174)) u_uart_tx
  (.i_Clock(clk),
   .i_Tx_DV(uart_tx_data_in_vld),
   .i_Tx_Byte(uart_tx_data_in),
   .o_Tx_Active(uart_tx_active),
   .o_Tx_Serial(uart_tx),
   .o_Tx_Done(uart_tx_done)
  );
  
// UART RX (Host PC --> Target)
localparam PMEM_WRITER_IDLE             = 3'b000;
localparam PMEM_WRITER_RST              = 3'b001;
localparam PMEM_WRITER_GET_LEN_AND_MODE = 3'b011;
localparam PMEM_WRITER_GET_START_ADDR   = 3'b010;
localparam PMEM_WRITER_WRITING          = 3'b110;
localparam PMEM_WRITER_DONE             = 3'b100;

logic   [2:0]       curr_pmem_writer_state;
logic   [2:0]       next_pmem_writer_state;
logic   [`ALEN-1:0] pmem_write_cntr;
logic   [`ALEN-1:0] pmem_write_len;
logic   [3:0]       pmem_write_mode; //  1 --> RST, 2 --> stream write, otherwise --> no write
logic   [`ALEN-1:0] pmem_write_start_addr;

logic   [1:0]       uart_rx_data_out_cntr; 
logic   [31:0]      uart_rx_word_out;
logic               uart_rx_data_out_vld;
logic   [7:0]       uart_rx_data_out;
logic   [7:0]       uart_rx_data_out_dline[3]; 

// Counter
always_ff @ ( posedge clk or negedge async_rst_n ) 
    if ( ~async_rst_n ) 
        uart_rx_data_out_cntr <= 0;
    else if ( sync_rst ) 
        uart_rx_data_out_cntr <= 0;
    else if ( uart_rx_data_out_vld ) 
        uart_rx_data_out_cntr <= uart_rx_data_out_cntr + 1;

// delay line
always_ff @ ( posedge clk or negedge async_rst_n )
    if ( ~async_rst_n ) begin
        uart_rx_data_out_dline[0] <= 0;
        uart_rx_data_out_dline[1] <= 0;
        uart_rx_data_out_dline[2] <= 0;
    end
    else if ( sync_rst ) begin
        uart_rx_data_out_dline[0] <= 0;
        uart_rx_data_out_dline[1] <= 0;
        uart_rx_data_out_dline[2] <= 0;
    end
    else if ( uart_rx_data_out_vld ) begin
        uart_rx_data_out_dline[0] <= uart_rx_data_out;
        uart_rx_data_out_dline[1] <= uart_rx_data_out_dline[0];
        uart_rx_data_out_dline[2] <= uart_rx_data_out_dline[1];      
    end   
                
// PMEM write counter (counts words, not bytes)
always_ff @ ( posedge clk or negedge async_rst_n )
    if ( ~async_rst_n ) 
        pmem_write_cntr <= 0;
    else if ( sync_rst | curr_pmem_writer_state ==  PMEM_WRITER_IDLE | curr_pmem_writer_state ==  PMEM_WRITER_DONE ) 
        pmem_write_cntr <= 0;
    else if ( uart_rx_word_out_vld & curr_pmem_writer_state == PMEM_WRITER_WRITING )
        pmem_write_cntr <= pmem_write_cntr + 1;
 
 // Get the length of PMEM write (pmem_write_len contains count of 32 bit words ****not bytes**** )      
 always_ff @ ( posedge clk or negedge async_rst_n )
    if ( ~async_rst_n ) begin
        pmem_write_len <= 0;
        pmem_write_mode <= 0;
    end
    else if ( sync_rst | curr_pmem_writer_state ==  PMEM_WRITER_IDLE ) begin
        pmem_write_len <= 0;
        pmem_write_mode <= 0;
    end
    else if ( uart_rx_word_out_vld & curr_pmem_writer_state == PMEM_WRITER_GET_LEN_AND_MODE ) begin
        pmem_write_len <= uart_rx_word_out[`ALEN-1:0];
        pmem_write_mode <= uart_rx_word_out[31:28];
    end

 // Get the start address of PMEM write       
 always_ff @ ( posedge clk or negedge async_rst_n )
    if ( ~async_rst_n ) 
        pmem_write_start_addr <= 0;
    else if ( sync_rst | curr_pmem_writer_state ==  PMEM_WRITER_IDLE ) 
        pmem_write_start_addr <= 0;
    else if ( uart_rx_word_out_vld  & curr_pmem_writer_state ==  PMEM_WRITER_GET_START_ADDR )
        pmem_write_start_addr <= uart_rx_word_out[`ALEN-1:0];
        
assign uart_rx_word_out_vld = uart_rx_data_out_vld & ( uart_rx_data_out_cntr == 3'b11 );
assign uart_rx_word_out = {uart_rx_data_out,uart_rx_data_out_dline[0],uart_rx_data_out_dline[1],uart_rx_data_out_dline[2]};

// PMEM Writer state machine
always_ff @ ( posedge clk or negedge async_rst_n )
    if ( ~async_rst_n )
        curr_pmem_writer_state <= PMEM_WRITER_IDLE;
    else if ( sync_rst )
        curr_pmem_writer_state <= PMEM_WRITER_IDLE;
    else 
        curr_pmem_writer_state <= next_pmem_writer_state;
        
always_comb begin
    
    case( curr_pmem_writer_state )
    
    PMEM_WRITER_IDLE: begin
    
        if ( uart_rx_word_out_vld & uart_rx_word_out == `UART_PMEM_WRITE_START_WORD )
            next_pmem_writer_state <= PMEM_WRITER_GET_LEN_AND_MODE;
        else
            next_pmem_writer_state <= PMEM_WRITER_IDLE;
            
    end
    
    PMEM_WRITER_GET_LEN_AND_MODE: begin
        
        if ( uart_rx_word_out_vld & uart_rx_word_out[31:28] == 1 )
            next_pmem_writer_state <= PMEM_WRITER_RST;
        else if ( uart_rx_word_out_vld & uart_rx_word_out[31:28] == 2 )
            next_pmem_writer_state <= PMEM_WRITER_GET_START_ADDR;
        else if ( uart_rx_word_out_vld )
            next_pmem_writer_state <= PMEM_WRITER_IDLE;
        else
            next_pmem_writer_state <= PMEM_WRITER_GET_LEN_AND_MODE;
    
    end

    PMEM_WRITER_RST: next_pmem_writer_state <= PMEM_WRITER_IDLE;
        
    PMEM_WRITER_GET_START_ADDR: begin
        
        if ( uart_rx_word_out_vld ) 
            next_pmem_writer_state <= PMEM_WRITER_WRITING;
        else
            next_pmem_writer_state <= PMEM_WRITER_GET_START_ADDR;
    
    end  
    
    PMEM_WRITER_WRITING: begin
        
        if ( uart_rx_word_out_vld & pmem_write_cntr == pmem_write_len - 1 ) 
            next_pmem_writer_state <= PMEM_WRITER_DONE;
        else
            next_pmem_writer_state <= PMEM_WRITER_WRITING;
    
    end     
    
    PMEM_WRITER_DONE: next_pmem_writer_state <= PMEM_WRITER_IDLE;
    
    default:
        next_pmem_writer_state <= PMEM_WRITER_IDLE;
    
    endcase

end

//RX
uart_rx #(.CLKS_PER_BIT(174)) u_uart_rx
  (.i_Clock(clk),
   .i_Rx_Serial(uart_rx),
   .o_Rx_DV(uart_rx_data_out_vld),
   .o_Rx_Byte(uart_rx_data_out)
   );

// Connect to PMEM write interface
assign start_programming_pulse  = ( curr_pmem_writer_state == PMEM_WRITER_GET_START_ADDR );
assign end_programming_pulse    = ( curr_pmem_writer_state == PMEM_WRITER_DONE );
assign prog_mem_sync_rst        = ( curr_pmem_writer_state == PMEM_WRITER_RST );
assign prog_mem_wr_en           = ( curr_pmem_writer_state == PMEM_WRITER_WRITING ) & uart_rx_word_out_vld;
assign prog_mem_addr            = pmem_write_start_addr + ( pmem_write_cntr << 2 );
assign prog_mem_wr_data         = uart_rx_word_out;
          
endmodule