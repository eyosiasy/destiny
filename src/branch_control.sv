`timescale 1ns / 1ps

module branch_control(
    input   [2:0]   branch_mode,  
    input   [7:0]   flag,
    
    
    output          branch
    );
    
// local copy of output ports
logic branch_local;
    
///////////////////////////////////////////////////
// Branch_mode  |   arithm mode |   code 
// ---------------------------------------
//  no branch   |               |  0
//  if ==       | signed        |  1
//  if !=       | signed        |  2
//  if <        | signed        |  3
//  if >=       | signed        |  4
//  if <        | unsigned      |  5
//  if >=       | unsigned      |  6
//  always      |               |  7  
////////////////////////////////////////////////////

//////////////////////////////////////////////////////////
//  conditions that set a bit in flag (from ALU) to 1 are
//   bit 0 --> alu_out == 0
//   bit 1 --> alu_in0 >= alu_in1
//   bit 2 --> alu_in0 == alu_in1
//   bit 3 --> alu_out >= 0
//   bit 4 --> overflow
//
///////////////////////////////////////////////////////////

always_comb begin
    if ( branch_mode == 3'h7 )
        branch_local <= 1;
    else if ( branch_mode == 3'h6 && flag[1] == 1 )
        branch_local <= 1;
    else if ( branch_mode == 3'h5 && flag[1] == 0 )
        branch_local <= 1;
    else if ( branch_mode == 3'h4 && flag[5] == 1 )
        branch_local <= 1;
    else if ( branch_mode == 3'h3 && flag[5] == 0 )
        branch_local <= 1;
    else if ( branch_mode == 3'h2 && flag[2] == 0 )
        branch_local <= 1;
    else if ( branch_mode == 3'h1 && flag[2] == 1 )
        branch_local <= 1;
    else 
        branch_local <= 0; 
end

// connect local signal to output port
assign branch = branch_local;

endmodule