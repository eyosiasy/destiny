`timescale 1ns / 1ps

`include "defines_inc.sv"

module registers(
    input               clk,
    input               async_rst_n,
    input               sync_rst,
    
    input   [4:0]       src0_addr,
    input   [4:0]       src1_addr,
    input   [4:0]       dest_addr,
    input               dest_wr_en,
    input   [`XLEN-1:0] dest_data,
    
    output  [`XLEN-1:0] src0_data,
    output  [`XLEN-1:0] src1_data
    );

// local copy of output ports
logic [`XLEN-1:0] src_data[2];
logic [`XLEN-1:0] src_data_local[2];
logic [4:0] src_addr_local[2];

// To allow write take priority over read
// src_addr is delayed within the block
// same is not true for dest_addr
logic [4:0]       src_addr_d[2];
  
// RISC-V I has 32 registers, each with bitwidth equal to XLEN 
logic [`XLEN-1:0] regs[32];

////////////////////////////////////////////////////////////////////
// Update destination register when dest_wr_en is high at posedge
// register 0 is always set at 0
////////////////////////////////////////////////////////////////////
assign src_addr_local[0] = src0_addr;
assign src_addr_local[1] = src1_addr;

genvar ii;

generate 

for ( ii = 1 ; ii < 32 ; ii++ ) begin

    always_ff @ ( posedge clk )
        if ( ~async_rst_n ) begin
            regs[ii] <= '{default: 0};
        end
        else if ( sync_rst ) begin
            regs[ii] <= '{default: 0};
        end
        else if ( dest_addr == ii & dest_wr_en ) begin
            regs[ii] <= dest_data;
        end
end

endgenerate

always_ff @ ( posedge clk or negedge async_rst_n )
    if ( ~async_rst_n ) begin
       regs[0] <= '{default: 0};
   end
   else if ( sync_rst ) begin
       regs[0] <= '{default: 0};
   end


//////////////////////////////////////////////////////////////////////////////////////////////
//
// There are two source registers
//
// If write and read arrive at port of this block at same clock write happens first
// this is ensured by adding one cycle delay on read path
//////////////////////////////////////////////////////////////////////////////////////////////
genvar jj;

generate

for ( jj = 0; jj < 2 ; jj++ ) begin

    always_ff @ ( posedge clk ) begin
        case( src_addr_d[jj] ) 
            0 : begin src_data_local[jj] <= regs[ 0]; end
            1 : begin src_data_local[jj] <= regs[ 1]; end
            2 : begin src_data_local[jj] <= regs[ 2]; end
            3 : begin src_data_local[jj] <= regs[ 3]; end 
            4 : begin src_data_local[jj] <= regs[ 4]; end
            5 : begin src_data_local[jj] <= regs[ 5]; end
            6 : begin src_data_local[jj] <= regs[ 6]; end
            7 : begin src_data_local[jj] <= regs[ 7]; end               
            8 : begin src_data_local[jj] <= regs[ 8]; end
            9 : begin src_data_local[jj] <= regs[ 9]; end
            10: begin src_data_local[jj] <= regs[10]; end
            11: begin src_data_local[jj] <= regs[11]; end 
            12: begin src_data_local[jj] <= regs[12]; end
            13: begin src_data_local[jj] <= regs[13]; end
            14: begin src_data_local[jj] <= regs[14]; end
            15: begin src_data_local[jj] <= regs[15]; end    
            16: begin src_data_local[jj] <= regs[16]; end
            17: begin src_data_local[jj] <= regs[17]; end
            18: begin src_data_local[jj] <= regs[18]; end
            19: begin src_data_local[jj] <= regs[19]; end 
            20: begin src_data_local[jj] <= regs[20]; end
            21: begin src_data_local[jj] <= regs[21]; end
            22: begin src_data_local[jj] <= regs[22]; end
            23: begin src_data_local[jj] <= regs[23]; end               
            24: begin src_data_local[jj] <= regs[24]; end
            25: begin src_data_local[jj] <= regs[25]; end
            26: begin src_data_local[jj] <= regs[26]; end
            27: begin src_data_local[jj] <= regs[27]; end 
            28: begin src_data_local[jj] <= regs[28]; end
            29: begin src_data_local[jj] <= regs[29]; end
            30: begin src_data_local[jj] <= regs[30]; end
            31: begin src_data_local[jj] <= regs[31]; end                    
        endcase
       
        src_addr_d[jj] <= src_addr_local[jj]; 
    end
        
     assign src_data[jj] = src_data_local[jj];
end

endgenerate

assign src0_data = src_data[0];
assign src1_data = src_data[1];

endmodule
